class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.belongs_to :product, index: true, foreign_key: true
      t.string :ip

      t.timestamps null: false
    end
  end
end
