# Disable logging
original_logger = ActiveRecord::Base.logger
ActiveRecord::Base.logger = nil #turns off sql activity

puts 'Generating seeds (~17MB)...'
i = 0;
(1..2).each do |company_id| # 2 comapnies
  (1..10).each do |p_number| # 2 * 10 = 20 products
    puts "Seeds: #{100 * i / 20}%"

    prod = Product.create!(name: "p#{p_number}c#{company_id}", company_id: company_id)

    ((Date.today - 20.days)..(Date.today + 10.days)).each do |date| # 31 days
      2.times do # UNION ALL does't support more than 500 unions
        visits_that_day = 100 + rand(300) # mean 250 visits/product/day, 500 counting twice
        strdate = date.strftime('%Y-%m-%d')

        sql = 'INSERT INTO visits(product_id, ip, created_at, updated_at) '
        sql += "SELECT #{prod.id} AS product_id, '127.0.0.1' AS ip, '#{strdate}' AS created_at, '#{strdate}' AS updated_at "
        visits_that_day.times do # 20 * 31 * 500 = 310.000 visits
          # SQLite does not support inserting multiple values until 3.7.11
          # We need to insert this way for performance :c
          sql += "UNION ALL SELECT #{prod.id}, '127.0.0.1', '#{strdate}', '#{strdate}' "
        end
        Visit.connection.execute(sql)
      end
    end
    i += 1
  end
end
puts 'Seeds: 100%!'

# Restore logging
ActiveRecord::Base.logger = original_logger
