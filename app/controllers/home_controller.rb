class HomeController < ApplicationController
  def index
    # We are not going to model companies, it's not part of the problem
    user_company_id = 1 # Assume this to be true

    # We need to count the visits to my company's products only
    products = Product.where(company_id: user_company_id)

    @visits_last_7_days = 0

    # Count for each company's product
    products.each do |product|
      product.visits.each do |visit|
        # Only visits from last 7 days
        if visit.created_at > 7.days.ago
          @visits_last_7_days += 1
        end
      end
    end
  end
end
